package main

import (
	"context"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
	"github.com/fvbommel/sortorder"
	"github.com/heroku/docker-registry-client/registry"
	"github.com/leaanthony/clir"
	"os"
	"regexp"
)

var verbose = false
var nagios  = false
var critical = 5
var warning = 1

func main()  {
	cli := clir.NewCli("docker-updater", "", "v0.0.1")

	// show container names and image hashes
	cli.BoolFlag("verbose", "Enable verbose output", &verbose)

	//config for nagios checks
	cli.BoolFlag("nagios", "suppress all output", &nagios)
	cli.IntFlag("c", "set critical value for nagios check", &critical)
	cli.IntFlag("w", "set warning value for nagios check", &warning)

	cli.Action(func() error {
		updatableContainers := 0

		// connect to docker daemon and get list of containers with label update.check
		dockerDaemon, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
		if err != nil {
			logError("couldn't connect to docker daemon")
			return err
		}

		containers, err := dockerDaemon.ContainerList(context.Background(), types.ContainerListOptions{
			Filters: filters.NewArgs(filters.Arg("label", "update.check")),
		})
		if err != nil {
			logError("couldn't list docker containers")
			return err
		}

		// check for every container, if there is a new image available on dockerhub
		for _, container := range containers {
			// lookup image tag & registry endpoint
			tag, err := lookupImageTag(container.Image)
			if err != nil {
				logError("couldn't lookup image tag for image %s", container.Image)
				return err
			}
			endpoint, err := tag.RegistryEndpoint()
			if err == ErrNoEndpointsFound {
				logWarn("no endpoints found for image %s; skipping.", container.Image)
				continue
			} else if err != nil {
				logError("couldn't request endpoints for image %s", container.Image)
				return err
			}
			logDebug("Using endpoint %s for image %s.", endpoint, container.Image)

			// connect to the registry
			registryClient, err := registry.New(endpoint, "", "")
			if err != nil {
				logError("couldn't create registry client")
				return err
			}
			registryClient.Logf = logDebug // seems like there's no way to hide the registry.ping message from registry.New :(

			// check if newer tag exists
			exists, updatedTag, err := getNewestTag(container.Labels["update.check"], tag, registryClient)
			if err != nil {
				logError("couldn't load tags")
				return err
			}
			if exists {
				logInfo("%s can be updated from %s to %s", container.Names[0], tag.Tag(), updatedTag)
				updatableContainers++
				addNewerImageToScript(container, tag.Tag(), updatedTag)
				addToUpdateScript(container)
				continue
			}

			// request the digest from the registry
			manifest, err := registryClient.ManifestV2(tag.Path(), tag.Tag())
			if err != nil {
				return err
			}
			logDebug("Image: %s - local hash: %s - remote hash: %s", container.Image, container.ImageID, manifest.Target().Digest.String())

			if container.ImageID != manifest.Target().Digest.String() {
				logInfo("%s can be updated", container.Names[0])
				updatableContainers++
				addToUpdateScript(container)
			} else {
				logDebug("%s is up to date", container.Names[0])
			}
		}

		logInfo("Number of updatable containers: %d", updatableContainers)
		saveUpdateScript()
		nagiosExit(updatableContainers)
		return nil
	})
	if err := cli.Run(); err != nil {
		logError("update check failed: %s\n", err)
		os.Exit(1)
	}
}


func getNewestTag(tagPattern string, tag NamedTagged, registryClient *registry.Registry ) (bool, string, error) {
	if tagPattern== "" {
		return false, "", nil
	}
	newTag := tag.Tag()
	// retrieve all tags for given image
	allTags, err := registryClient.Tags(tag.Path())
	if err != nil {
		return false, "", err
	}

	logDebug("Using tag pattern %s for image %s", tagPattern, tag.Path())
	tagPatternRegex := regexp.MustCompile(tagPattern)
	for _, t := range allTags {
		// test if matching tags is newer then current and current newer tag
		if tagPatternRegex.MatchString(t) && sortorder.NaturalLess(tag.Tag(), t) && sortorder.NaturalLess(newTag, t) {
			newTag = t
		}
	}
	logDebug("Newest tag is %s", newTag)
	if newTag == tag.Tag() {
		return false, "", nil
	}
	return true, newTag, nil
}