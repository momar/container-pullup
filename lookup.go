package main

import (
	"errors"
	"github.com/docker/distribution/reference"
	"github.com/docker/docker/registry"
	"strings"
)

// create default service to lookup pull URLs based on image names
var registryService, registryServiceErr = registry.NewService(registry.ServiceOptions{})

var ErrNoEndpointsFound = errors.New("no endpoints found")

func init() {
	if registryServiceErr != nil {
		panic(registryServiceErr)
	}
}

type NamedTagged struct {
	reference.NamedTagged
}

func lookupImageTag(imageName string) (NamedTagged, error) {
	name, err := reference.ParseNormalizedNamed(imageName)
	if err != nil {
		return NamedTagged{}, err
	}
	firstSlash := strings.IndexRune(imageName, '/')+1
	tagSep := strings.IndexRune(imageName[firstSlash:], ':')
	if reference.IsNameOnly(name) && tagSep > -1 {
		// TODO: is this required?!
		tag, err := reference.WithTag(name, imageName[firstSlash+tagSep+1:])
		return NamedTagged{tag}, err
	} else {
		return NamedTagged{reference.TagNameOnly(name).(reference.NamedTagged)}, nil
	}
}

func (tag NamedTagged) RegistryEndpoint() (string, error) {
	endpoints, err := registryService.LookupPullEndpoints(tag.Domain())
	if err != nil {
		return "", err
	}
	if len(endpoints) < 1 {
		return "", ErrNoEndpointsFound
	}
	return endpoints[0].URL.String(), nil
}

func (tag NamedTagged) Domain() string {
	return reference.Domain(tag.NamedTagged)
}

func (tag NamedTagged) Path() string {
	return reference.Path(tag.NamedTagged)
}