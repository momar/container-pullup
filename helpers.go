package main

import (
	"fmt"
	"os"
)

func logDebug(format string, args ...interface{}) {
	if verbose {
		fmt.Printf("[DEBUG] "+format+"\n", args...)
	}
}
func logInfo(format string, args ...interface{}) {
	if !nagios || verbose {
		fmt.Printf("[INFO ] " + format + "\n", args...)
	}
}
func logWarn(format string, args ...interface{}) {
	if !nagios || verbose {
		fmt.Printf("[WARN ] "+format+"\n", args...)
	}
}
func logError(format string, args ...interface{}) {
	if !nagios || verbose{
		fmt.Printf("[ERROR] " + format + "\n", args...)
	}
}

func nagiosExit(updatable int) {
	if !nagios {return}
	fmt.Printf("%d Containers can be updated.\n", updatable)
	if updatable >= critical {
		//Exit Error
		os.Exit(2)
	}
	if updatable >= warning {
		//Exit Warning
		os.Exit(1)
	}
}
