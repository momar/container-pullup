# Container Pullup

A semi-automatic update solution for e. g. Docker Compose projects with notifications, tag selection, maintenance windows, rollbacks, and much more.

### ⚠️ WARNING: README DRIVEN DEVELOPMENT

**This repository is using [Readme Driven Development](https://tom.preston-werner.com/2010/08/23/readme-driven-development.html), which means that everything you can read here are still ideas and plans for the future! The first public release will be some time in 2021.**

---

# Roadmap

- [x] check if new image with same tag is available
- [x] check if new tag is available in the specified format
- [x] nagios-check for monitoring the number of updatable containers
- [ ] Notification via Email
- [ ] Rollback possibility
- [ ] create update script

# Usage

## Nagios
Container-pullup can be used as a nagios check. 
```
container-pullup -nagios [-c 5] [-w 1] 2> /dev/null
```
Warning (`-w`) and critical (`-c`) value can be changed from the default values via the commandline. 
One used package logs to stderr without a possibility to deactivate the logging, so to suppress the log, pipe stderr to /dev/null

## Interactive Update
This will show you a list of version changes and lets you confirm each container/image update.
Projects will be updated atomically, which means that after every updated project, you can test if it works and roll back the changes if it doesn't.
Mounting the folder of your `docker-compose.yml` files under the same path will allow them files to be updated automatically.

```
docker run \
  --rm -it \
  -v "/var/run/docker.sock:/var/run/docker.sock" \
  -v "/mnt/projects:/mnt/projects" \
  codeberg.org/momar/container-pullup
```

1. Show diff of updates, let you choose which containers you want to update
2. For each project: update everything, wait for confirmation or rollback command
3. If confirmed, update the corresponding `docker-compose.yml` file in the project subdirectory in `/mnt/projects/`

## Fully Autonomous Updates
This will automatically update all your containers within the optionally specified maintenance windows (using the time zone from the environment variable `TZ`, or UTC if it's not set).
If you specify the environment variables `SMTP_SERVER`, `SMTP_USERNAME`, `SMTP_PASSWORD`, `SMTP_FROM`, `SMTP_TO` and `ROOT_URL` (there will be a web server listening on port 80 in the container), you will receive an email 30 minutes before an update is done, with the option to hold it back or run it manually.
```
docker run \
  -d --restart=always \
  -v "/var/run/docker.sock:/var/run/docker.sock" \
  -v "/mnt/projects:/mnt/projects" \
  -e SMTP_SERVER=smtp.postmarkapp.com:587 \
  -e SMTP_USERNAME=secret -e SMTP_PASSWORD=secret \
  -e SMTP_FROM=noreply@example.org -e SMTP_TO=admin@example.org \
  -e ROOT_URL=http://myserver.example.org \
  -e TZ="$(timedatectl show -p Timezone | tail -c+10)" \
  codeberg.org/momar/container-pullup \
  autonomous "mo-mi,fr 04:40 + 1h 30m" "sa,su 00:00 + 24h"
```

## Labels
You should add a `update.check` label to your containers containing a regular expression for the tags you want to consider for updates:
```yaml
version: "3"
services:
  database:
    image: mariadb:10.5
    labels:
      update.check: '^10.\d+$'
```

**If you don't add an `update.check` label to a container, it will not consider that container for update checks!**

The latest tag will be determined as follows:
- Sort the tags without any numbers (`\d+` gets replaced with `0`) alphanumerically
- For all tags with the same position, sort by number groups

For example, this means that the following sort order will be used:
- `v1-alpha2`
- `v1-alpha10`
- `v1-beta1`
- `v2`
- `v2-alpha8`
- `v2.3`
- `v10` (newest)
