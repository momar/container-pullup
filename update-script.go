package main

import (
	"fmt"
	"github.com/docker/docker/api/types"
	"path/filepath"
	"strings"
)

var updateScript = "#!/bin/bash\n\n// This script was automatically created. Use to own risk!\n// Source of the tool can be found at https://coderberg.org/momar/container-pullup\n\n"

//ToDo: Update docker-compose imageTag

func addToUpdateScript(container types.Container) {
	dockerComposeFile := filepath.Join(container.Labels["com.docker.compose.project.working_dir"], container.Labels["com.docker.compose.project.config_files"])
	serviceName := container.Labels["com.docker.compose.service"]
	updateScript += fmt.Sprintf("echo updating service %s\n", strings.TrimPrefix(container.Names[0], "/"))
	updateScript += fmt.Sprintf("docker-compose -f %s pull %s\n", dockerComposeFile, serviceName)
	updateScript += fmt.Sprintf("docker-compose -f %s up -d %s\n", dockerComposeFile, serviceName)
}

func addNewerImageToScript(container types.Container, oldTag, newTag string) {
	dockerComposeFile := filepath.Join(container.Labels["com.docker.compose.project.working_dir"], container.Labels["com.docker.compose.project.config_files"])
	image := strings.Split(container.Image, ":")[0]
	updateScript += fmt.Sprintf("# Updating Image %s for Service %s from tag %s to %s\n", image, container.Names[0], oldTag, newTag)
	updateScript += fmt.Sprintf("sed -i.bak 's#%s:%s#%s:%s#g' %s\n", image, oldTag, image, newTag, dockerComposeFile)
}

func saveUpdateScript() {
	logDebug("File output:\n %s", updateScript)
	//ioutil.WriteFile("/tmp/", []byte(updateScript), 0777)
}
